﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMove : MonoBehaviour {

	private new Rigidbody rigidbody;
	void Start () {
		rigidbody = GetComponent <Rigidbody> ();
		rigidbody.useGravity = false;
	}

	public Vector2 move;
	public Vector2 velocity;

	public float speed = 20f;
	public float force = 10f;

	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");

		Vector2 velocity = direction * force * speed;
		rigidbody.AddForce (velocity * Time.deltaTime);
	}
}
